# Projet Recalage Image

Projet réalisé dans le cadre de l'UE "Initation à la recherche".

Encadré par [HERBULOT A.](https://homepages.laas.fr/aherbulo/)

Ce projet consiste à implémenter une méthode de recalage d'image pour la création de mosaïques d'images. Cette méthode consiste à extraire des caractéristiques communes entre les images pour assurer leur alignement correct. Pour le recalage, l'homographie a été utilisée se qui rend le programme en lui même moins robuste dû au fait qu'elle ne concidère pas la géométrie 3-dimentionnelle d'une scène.

---

## Utilisation
Le dépôt contient un fichier `Pipfile` qui fournit un environnement dans lequel les bibliothèques utilisées sont déjà installées. Pour entrer dans cet environnement avec la ligne de commande utilisez la commande suivante :

`pipenv shell`

Le fichier à executer : `main.py`

```
usage: ImageRegistrator [-h] -l LEFT -r RIGHT -o OUTPUT

Create a mosaic from images

options:
  -h, --help                    show this help message and exit
  -l LEFT, --left LEFT          Path to the left image
  -r RIGHT, --right RIGHT       Path to the right image
  -o OUTPUT, --output OUTPUT    Path to write the output image
```

---
## Documents utiles
[Sujet](https://gitlab.com/konstantinosmelis/projet-recalage-image/-/blob/main/sujet_herbulot.pdf)

[Rapport écrit](https://gitlab.com/konstantinosmelis/projet-recalage-image/-/blob/main/rapport.pdf)

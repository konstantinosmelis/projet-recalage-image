import sys
import utils
import argparse

def main(left_img, right_img, output):
    f2, f1, matches = utils.match(right_img, left_img)
    H, inliners = utils.ransac(matches, 5)
    print(f"Final number of inliners: {len(inliners)}")
    projection = utils.project_image(f2.image, H)
    mosaic = utils.stich_images(f1.image, projection)
    utils.cv2.imwrite(output, mosaic)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='ImageRegistrator', description='Create a mosaic from images')
    parser.add_argument('-l', '--left', help='Path to the left image', required=True)
    parser.add_argument('-r', '--right', help='Path to the right image', required=True)
    parser.add_argument('-o', '--output', help='Path to write the output image', required=True)
    args, _ = parser.parse_known_args()
    main(args.left, args.right, args.output)

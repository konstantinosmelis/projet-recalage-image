import cv2
import subprocess
import random
import numpy as np
import Features

def sift(filename):
    image = cv2.imread(filename)
    rows, cols, _ = image.shape
    with open("tmp.pgm", "w") as file:
        file.write(f"P5\n{cols}\n{rows}\n255\n")
        cv2.imwrite("tmp.pgm", cv2.cvtColor(image, cv2.COLOR_RGB2GRAY))
        file.close()
    subprocess.run("./sift < tmp.pgm > tmp.key", shell=True)
    features = Features.Features(image, [], [])
    with open("tmp.key") as g:
        num, length = list(map(int, next(g).split(" ")))
        if int(length) != 128:
            raise Exception("Keypoint descriptor length invalid (should be 128)")
        for i in range(num):
            vec = list(map(float, next(g).split(" ")))
            features.locations.append(list(map(int, vec)))
            li = []
            for j in range(7):
                descr = list(map(float, next(g).split(" ")[1:]))
                li += descr
            features.descriptors.append(li)
        g.close()
    subprocess.call(["rm", "tmp.key", "tmp.pgm"])
    features.descriptors = np.array(features.descriptors)
    features.locations = np.array(features.locations)
    return features


def match(image1, image2):
    f1 = sift(image1)
    f2 = sift(image2)
    matches = []
    for i in range(len(f1.descriptors)):
        m = check_for_match(f1.descriptors[i], f2)
        if m is not None:
            loc = m[1]
            matches.append((f1.locations[i], loc))
    print(f"Found {len(matches)} matches for images \"{image1}\" and \"{image2}\"")
    return f1, f2, matches


def check_for_match(desc, f2):
    dsq = 0
    dsq1 = dsq2 = np.Inf
    min_d_loc = None
    for i in range(len(f2.descriptors)):
        d = f2.descriptors[i]
        dsq = np.sum((desc - d)**2)
        if dsq < dsq1:
            dsq2 = dsq1
            dsq1 = dsq
            min_d_loc = f2.locations[i]
        elif dsq < dsq2:
            dsq2 = dsq
    if dsq1 < .36 * dsq2:
        return (d, min_d_loc)
    return None


def append_images(img1, img2):
    rows1 = img1.shape[0]
    rows2 = img2.shape[0]
    if rows1 < rows2:
        img_t = np.zeros((rows2, img1.shape[1], img1.shape[2]))
        for i in range(rows1):
            img_t[i, :, :] = img1[i, :, :]
        img1 = img_t
    else:
        img_t = np.zeros((rows1, img2.shape[1], img1.shape[2]))
        for i in range(rows2):
            img_t[i, :, :] = img2[i, :, :]
        img2 = img_t
    return np.hstack((img1, img2))


def construct_A(matches):
    A = np.zeros((2 * len(matches), 9), dtype=np.float32)
    for i in range(len(matches)):
        x, y = matches[i][0][0], matches[i][0][1]
        u, v = matches[i][1][0], matches[i][1][1]
        A[2 * i] = [x, y, 1, 0, 0, 0, -x * u, -y * u, -u]
        A[2 * i + 1] = [0, 0, 0, x, y, 1, -x * v, -y * v, -v]
    return A


def homography(match_set):
    A = construct_A(match_set)
    _, _, V = np.linalg.svd(A, full_matrices=True)
    H = V[-1].reshape((3, 3))
    return H / H[2, 2]


def dist(matches, homography):
    p1 = np.array([matches[0][0], matches[0][1], 1])
    p2 = np.array([matches[1][0], matches[1][1], 1])
    p2_est = np.dot(homography, p1)
    p2_est = p2_est / p2_est[2]
    return np.linalg.norm(p2 - p2_est)


def ransac(matches, max_dist, threshold=.6):
    H = None
    maxInliners = []
    for i in range(500):
        inliners = []
        match_set = []
        for j in range(4):
            k = random.randint(0, len(matches) - 1)
            match_set.append((matches[k][0][0:2], matches[k][1][0:2]))
        h = homography(match_set)
        for j in range(len(matches)):
            d = dist(matches[j], h)
            if d < max_dist:
                inliners.append(matches[j])
        if len(inliners) > len(maxInliners):
            maxInliners = inliners
            H = h
        if len(maxInliners) > len(matches) * threshold:
            break
    return H, np.array(maxInliners)


def project_image(img, homography):
    projection = np.zeros((img.shape[0], 2 * img.shape[1], img.shape[2]))
    maxy = 0
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            p = np.array([i, j, 1])
            p_est = np.dot(homography, p)
            p_est = p_est / p_est[2]
            if p_est[0] >= projection.shape[0] or p_est[1] >= projection.shape[1]:
                continue
            projection[int(p_est[0]), int(p_est[1]), :] = img[i, j, :]
            maxy = max(maxy, int(p_est[1]))
    return np.delete(projection, np.s_[maxy:], axis=1)


def stich_images(img1, img2):
    result = np.zeros((img2.shape[0], img2.shape[1], img2.shape[2]))
    h = min(img1.shape[0], img2.shape[0])
    w = img1.shape[1]
    result[:h, :w, :] = img1[:h, :w, :]
    result[:, w:, :] = img2[:, w:, :]
    return result
